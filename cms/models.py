from django.db import models


# Create your models here.
class Banner(models.Model):
    caption = models.CharField(max_length=200, blank=True)
    image = models.ImageField()

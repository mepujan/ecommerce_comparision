from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=20)
    slug = AutoSlugField(populate_from='title')

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=20)
    slug = AutoSlugField(populate_from='title')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField()
    brand_name = models.CharField(max_length=20)
    price = models.FloatField(blank=True)
    details = models.TextField(blank=True)


class ProductAttribute(models.Model):
    key = models.CharField(max_length=20)
    value = models.CharField(max_length=50)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)


class ProductReview(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    rating = models.IntegerField()
    comment = models.TextField()

    def rating_range(self):
        return range(self.rating)


class Scrapy(models.Model):
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    daraz_price = models.FloatField(null=True)
    sastodeal_price = models.FloatField(null=True)
    esewa_price = models.FloatField(null=True)
    date_time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Scrapies'

    def __str__(self):
        return self.products.title


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty = models.IntegerField()

    def total(self):
        return self.product.price * self.qty

    def __str__(self):
        return self.product.title


class AboutUs(models.Model):
    content = RichTextField()
    pub_date = models.DateField(auto_now=True)

    class Meta:
        verbose_name_plural = 'AboutUs'

    def __str__(self):
        return self.pub_date



import self as self
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from cms.models import Banner
from .models import *
from django.views import View
from .models import Category, Product, Scrapy, Cart
from django.contrib.auth.forms import UserCreationForm
from .forms import ProductReviewForm, SignUpForm
from django.db.models import Q
from django.contrib import messages
from django.http import HttpResponseRedirect
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from datetime import datetime
import os
from django.core.paginator import Paginator
from django.views.generic import ListView

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Comparision00.settings')

import django

django.setup()


# Create your views here.

def homepage(request):
    categories = Category.objects.all()
    banners = Banner.objects.all()
    products = Product.objects.order_by('-id')[:10]
    paginator = Paginator(products, 1)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    context = {
        'categories': categories,
        'banners': banners,
        'posts': posts,

    }
    return render(request, 'homepage.html', context)


def category_wise(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    categories = Category.objects.all()

    context = {
        'categories': categories,
        'category': category,
        'products': category.product_set.all()
    }

    return render(request, 'category_wise.html', context)


def product_detail(request, product_slug):
    # def product_detail(request, id):
    # product = get_object_or_404(Product, id=
    product = Product.objects.get(slug=product_slug)
    categories = Category.objects.all()
    # scrapy = get_object_or_404(Scrapy, id=id)
    form = ProductReviewForm()

    if request.method == 'POST':
        form = ProductReviewForm(request.POST)

        if form.is_valid():
            review = form.save(commit=False)
            review.product = product
            review.user = request.user
            review.save()

    context = {
        'product': product,
        'categories': categories,
        'form': form,

    }

    return render(request, 'product-detail.html', context)


def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = SignUpForm()

    context = {
        'form': form
    }

    return render(request, 'registration/register.html', context)


def search(request):
    if request.method == 'POST':
        search_text = request.POST['search']
        products = Product.objects.filter(Q(title__contains=search_text))

        if products:
            context = {
                'search_text': search_text,
                'products': products
            }
            return render(request, 'search.html', context)
        else:
            messages.error(request, 'No result found !!')
    else:
        return HttpResponseRedirect('/homepage/')
    return render(request, 'homepage.html')


def products(request):
    products = Product.objects.order_by('-id')[:10]
    context = {

        'products': products
    }

    return render(request, 'products.html', context)


def scrap_fun(request, id):
    product_obj = get_object_or_404(Product, id=id)
    print(product_obj.title)

    product_name = product_obj.title

    product_name = product_name.lower()
    style1 = product_name.replace(' ', '%20')
    style2 = product_name.replace(' ', '+')

    ### For scrapping from daraz.com.np
    d_browser = webdriver.PhantomJS()
    request = d_browser.get("https://www.daraz.com.np/catalog/?q={}&sort=priceasc".format(style1))
    page = BeautifulSoup(d_browser.page_source, "html.parser")
    element = page.find("span", {"class": "c13VH6"})
    if element is not None:
        d_price_string = element.text.strip()
        d_price_num = float(d_price_string[4:].replace(',', ''))
        print("Price @ Daraz is Rs. {}".format(d_price_num))
    else:
        d_price_num = None
        print("Product Not Available @ SastoDeal")
    d_browser.close()

    ### For scrapping from sastodeal.com
    s_browser = webdriver.PhantomJS()
    request = s_browser.get(
        "https://www.sastodeal.com/search.html?q={}&idx=sastodeal_products_price_ascending".format(style1))
    page = BeautifulSoup(s_browser.page_source, "html.parser")
    element = page.find("div", {"class": "product-price"})
    if element is not None:
        s_price_string = element.text.split()[1]
        s_price_num = float(s_price_string[:-2].replace(',', ''))
        print("Price @ SastoDeal is Rs. {}".format(s_price_num))
    else:
        s_price_num = None
        print("Product Not Available @ SastoDeal")
    s_browser.close()

    ### For scrapping from esewapasal.com
    request = requests.get(
        "https://www.esewapasal.com/catalogsearch/result/index/?product_list_order=price&q={}&product_list_dir=asc".format(
            style2))
    soup = BeautifulSoup(request.content, "html.parser")
    element = soup.find("span", {"class": "price"})
    if element is not None:
        e_price_string = element.text.strip()
        e_price_num = float(e_price_string[4:].replace(',', ''))
        print("Price @ eSewaPasal is Rs. {}".format(e_price_num))
    else:
        e_price_num = None
        print("Product Not Available @ eSewaPasal")

    # d = datetime.date()
    # print(d)

    # e_product = Scrapy(esewa_price=e_price_num)
    # e_product.save()
    # print(e_price_num)
    if d_price_num or s_price_num or e_price_num:
        # messages.success(request, 'Crawling successful')
        prices = Scrapy(daraz_price=d_price_num, sastodeal_price=s_price_num, esewa_price=e_price_num, products_id=id)
        prices.save(force_insert=True)
        return HttpResponseRedirect('/products')

    else:
        messages.error(request, 'Error in crawling the data')

    return redirect('/products')


class BaseView(View):
    template_context = {
        'categories': Category.objects.order_by('title').all(),
        # here if we give Category.objects.all() it will load but without ordering by title

    }


class CartView(LoginRequiredMixin,
               BaseView):

    def get(self, request):
        self.template_context['cart_items'] = Cart.objects.filter(user=request.user).order_by('-id')
        self.template_context['latest_products'] = Product.objects.order_by('-id')[:5]
        self.template_context['intrested_products'] = Product.objects.order_by('?')[:2]

        total_amount = 0

        for cart_item in self.template_context['cart_items']:
            total_amount += cart_item.total()
        self.template_context['total_amount'] = total_amount
        return render(request, 'cart.html', self.template_context)

    def post(self, request):
        item = Cart()
        product = Product.objects.get(pk=request.POST.get('product_id'))
        item.product = product
        item.qty = request.POST.get('qty')
        item.user = request.user
        item.save()
        return redirect('/cart')


def remove(request, id):
    Cart.objects.filter(id=id).delete()
    return redirect('/cart')


class ChartDisplay(BaseView):
    model = Product
    template_name = 'shop/products.html'

    def get_context_data(self, **kwargs):
        context = super(ChartDisplay, self).get_context_data(*kwargs)
        print(context)
        priceDate = Scrapy.objects.filter(product__Scrapy__product=context['product']).distinct()
        date = list(Scrapy.value_list('date_time', flat=True).distinct().order_by('date_time'))
        price = list(Scrapy.values_list('price', flat=True).order_by('date_time').distinct())
        context['data'] = zip(date, price)
        return context

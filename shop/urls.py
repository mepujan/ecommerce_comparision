from django.urls import path
from .views import *

# homepage, category_wise, product_detail, register, search

app_name = 'shop'

urlpatterns = [
    path('', homepage, name='homepage'),
    path('category/<str:category_slug>', category_wise, name='category_wise'),
    path('register', register, name='register'),
    path('product/<str:product_slug>', product_detail, name='product_detail'),
    path('search', search, name='search'),
    path('products', products, name='products'),
    path('scrape_fun/<int:id>', scrap_fun, name='scrape_fun'),
    path('cart', CartView.as_view(), name='cart'),
    path('remove_product/<int:id>', remove, name='remove_product')
]

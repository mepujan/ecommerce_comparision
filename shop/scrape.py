import requests
from bs4 import BeautifulSoup
from selenium import webdriver

product_name = 'samsung '

product_name = product_name.lower()
style1 = product_name.replace(' ', '%20')
style2 = product_name.replace(' ', '+')

### For scrapping from daraz.com.np
d_browser = webdriver.PhantomJS()
request = d_browser.get("https://www.daraz.com.np/catalog/?q={}&sort=priceasc".format(style1))
page = BeautifulSoup(d_browser.page_source, "html.parser")
element = page.find("span", {"class": "c13VH6"})
if element is not None:
    d_price_string = element.text.strip()
    d_price_num = float(d_price_string[4:].replace(',', ''))
    print("Price @ Daraz is Rs. {}".format(d_price_num))
else:
    d_price_num = None
    print("Product Not Available @ SastoDeal")
# print(d_price_num)
d_browser.close()

### For scrapping from sastodeal.com
s_browser = webdriver.PhantomJS()
request = s_browser.get(
    "https://www.sastodeal.com/search.html?q={}&idx=sastodeal_products_price_ascending".format(style1))
page = BeautifulSoup(s_browser.page_source, "html.parser")
element = page.find("div", {"class": "product-price"})
if element is not None:
    s_price_string = element.text.split()[1]
    s_price_num = float(s_price_string[:-2].replace(',', ''))
    print("Price @ SastoDeal is Rs. {}".format(s_price_num))
else:
    s_price_num = None
    print("Product Not Available @ SastoDeal")
# print(s_price_num)
s_browser.close()

### For scrapping from esewapasal.com
request = requests.get(
    "https://www.esewapasal.com/catalogsearch/result/index/?product_list_order=price&q={}&product_list_dir=asc".format(
        style2))
soup = BeautifulSoup(request.content, "html.parser")
element = soup.find("span", {"class": "price"})
if element is not None:
    e_price_string = element.text.strip()
    e_price_num = float(e_price_string[4:].replace(',', ''))
    print("Price @ eSewaPasal is Rs. {}".format(e_price_num))
else:
    e_price_num = None
    print("Product Not Available @ eSewaPasal")
# print(e_price_num)

from django.contrib import admin
from .models import Category, Product, ProductAttribute, Scrapy


# Register your models here.
class ProductAttributeAdminInline(admin.TabularInline):
    model = ProductAttribute


class ProductAdmin(admin.ModelAdmin):
    inlines = (ProductAttributeAdminInline,)
    change_list = 'admin/change_list.html'
    search_fields = ('title', 'category', 'price')


admin.site.register(Product, ProductAdmin)

admin.site.register(Scrapy)

# Register your models here.


admin.site.register(Category)
